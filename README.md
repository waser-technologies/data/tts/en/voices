# Bunch of high quality voices

They are produced with TTS vctk/vits for english.

They can be used with yourTTS to quickly clone them for real time.

Checkout [`female`](female/) and [`male`](male/) directories.

`default.wav` is just a copy of my favorite.

## Audio samples
<audio src="https://gitlab.com/waser-technologies/data/tts/en/voices/-/raw/master/female/default.wav?inline=false" controls preload></audio>
![](female/default.wav)

<audio src="https://gitlab.com/waser-technologies/data/tts/en/voices/-/raw/master/male/default.wav?inline=false" controls preload></audio>
![](male/default.wav)

<audio src="https://gitlab.com/waser-technologies/data/tts/en/voices/-/raw/master/female/default_2.wav?inline=false" controls preload></audio>
![](female/default_2.wav)