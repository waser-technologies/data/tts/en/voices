#!/usr/bin/env zsh

# Download all clips of OS1 from the trailer of the movie "Her" and make one reference wav file. 
# This script downloads audio from a YouTube video and extracts specific segments from it.
# The segments are then concatenated and converted to mono wav file.
# The script uses yt-dlp to download the audio, and ffmpeg plus sox to edit the audio.

set -e

# The source media and the segments to extract from it
vid_url="https://youtu.be/f9Hg1x-Ctlw?si=FivvE70_Yx_hYnHF"
segments=(
    "00:00:12.5 00:00:26.45"
    "00:00:27 00:00:30"
    "00:00:34.05 00:00:37.5"
    "00:00:39 00:00:40"
    "00:00:44.5 00:00:48"
    "00:00:51 00:00:54.1"
    "00:01:08.3 00:01:13"
)

concat_wav() {
    input_dir=$1
    # _input_files=$(find $input_dir -name '*.wav' -print)
    
    output_file=$2

    echo "Input directory: $input_dir"
    # echo "Input files: $_input_files"
    echo "Output file: $output_file"

    local editor=$(which sox)

    if [ -z $editor ]; then
        echo "sox not found. Please install it"
        exit 1
    fi

    # input_files=""
    # input_filters=""
    # i=0
    # for file in ${input_dir}/*.wav; do
    #     if [ -z $input_files ]; then
    #         input_files="-i ${file}"
    #     else
    #         input_files="${input_files} -i ${file}"
    #     fi
    #     input_filters+="[$i:0]"
    #     i=$((i+1))
    # done

    # echo "Input files: $input_files"
    # echo "Input filters: $input_filters"
    # echo "Number of input files: $i"
    # Concatenate the input files using FFmpeg
    # ffmpeg -filter_complex \
    #     "${input_filters}concat=n=${i}:v=0:a=1[out]" \
    #     -map "[out]" \
    #     ${input_files} \
    #     "$output_file"
    input_files="${input_dir}/*.wav"
    $editor $input_files $output_file

    echo "Concatenated audio saved as $output_file"
}

build() {
    
    # Make sure we have one argument
    if [ $# -ne 1 ]; then
        echo "Usage: build <wav_path>"
        return 1
    fi

    echo "Preapring..."
    local output_wav=$1

    local downloader=$(which yt-dlp)

    echo "Downloading audio from $vid_url"
    
    if [ -z $downloader ]; then
        echo "yt-dlp not found. Please install it"
        exit 1
    fi
    
    local tmp_dir="$(dirname $output_wav)/tmp_$(basename $output_wav | rev | cut -d'.' -f2- | rev)"
    
    echo "Temp Dir: ${tmp_dir}"
    mkdir -p $tmp_dir
    
    # Iterate over the segments array
    # for segment in "${segments[@]}"; do
    #     start_time=$(echo "$segment" | cut -d' ' -f1)
    #     end_time=$(echo "$segment" | cut -d' ' -f2)
        
    #     _output_wav="${tmp_dir}/$(echo $start_time | tr ':' '-')_$(echo $end_time | tr ':' '-').wav"
    #     # check if the file exists
    #     if [ -f $_output_wav ]; then
    #         echo "File $(basename $_output_wav): already downloaded"
    #         continue
    #     fi
    #     echo "Downloading segment: $start_time - $end_time using $downloader"
    #     $downloader -f bestaudio --extract-audio --audio-format wav --audio-quality 0 -k --output $_output_wav --external-downloader aria2c --external-downloader-args "-x 16 -s 16" --postprocessor-args "-ss $start_time -to $end_time" "${vid_url}"
    # done
    local _output_wav="${tmp_dir}/source.wav"
    if [ ! -f $_output_wav ]; then
        echo "Downloading data..."
        $downloader -f bestaudio --extract-audio --audio-format wav --audio-quality 0 -k --output $_output_wav --external-downloader aria2c --external-downloader-args "-x 16 -s 16" "${vid_url}"
        if [ -n "$(find $tmp_dir -name '*.orig.wav' -print -quit)" ]; then
            rm -f $tmp_dir/*.orig.wav
        fi
    else
        echo "File $(basename $_output_wav): already downloaded"
    fi

    # check if orig.wav files exist and remove them

    # split segments

    echo "Editing audio..."
    local editor=$(which ffmpeg)

    if [ -z $editor ]; then
        echo "ffmpeg not found. Please install it"
        exit 1
    fi

    mkdir -p "${tmp_dir}/edit"

    for segment in "${segments[@]}"; do
        start_time=$(echo "$segment" | cut -d' ' -f1)
        end_time=$(echo "$segment" | cut -d' ' -f2)
        segment_output_wav="${tmp_dir}/edit/$(echo $start_time | tr ':' '-')_$(echo $end_time | tr ':' '-').wav"
        # check if the file exists
        if [ -f $segment_output_wav ]; then
            echo "File $(basename $segment_output_wav): already downloaded"
            continue
        fi
        echo "Editing segment: $start_time - $end_time using $editor"
        $editor -i $_output_wav -ss $start_time -to $end_time -c copy $segment_output_wav
    done

    local stero_output_wav_tmp=$(dirname $output_wav)/os_tmp_stereo.wav

    echo "Concatenating above segments..."
    concat_wav "${tmp_dir}/edit" $stero_output_wav_tmp || (echo "Failed to concatenate audio" && exit 1)
    # concat_exit_code=$?
    # if [ concat_exit_code -ne 0 ]; then
    #     echo "Failed to concatenate audio"
    #     return 1
    # else
    #     echo "Concatenated audio successfully"
    # fi

    echo "Converting to mono..."
    ffmpeg -i $stero_output_wav_tmp -ac 1 $output_wav
    local ffmpeg_exit_code=$?
    if [ ffmpeg_exit_code -ne 0 ]; then
        echo "Failed to convert audio to mono"
        exit 1
    else
        echo "Converted audio to mono successfully"
    fi

    echo "Cleaning up..."
    rm -rf $tmp_dir
    rm -f $stero_output_wav_tmp

    echo "Audio saved as $output_wav"
    soxi $output_wav

    return 0
}

install() {
    # Make sure we have one argument
    if [ $# -ne 1 ]; then
        echo "Usage: install <wav_path>"
        exit 1
    fi

    local output_wav=$1

    # Check if the file exists
    if [ ! -f $output_wav ]; then
        echo "File $output_wav does not exist; installing..."
        mkdir -p $(dirname $output_wav)
        echo "Building audio..."
        build $output_wav
        # build_exit_code=$?
        # if [ build_exit_code -ne 0 ]; then
        #     echo "Failed to build audio"
        #     return 1
        # else
        #     echo "Built audio successfully"
        # fi
    else
        echo "File $output_wav exists"
        exit 2  
    fi

    return 0
}

install $1
# install_exit_code=$?
# if [ install_exit_code -eq 2 ]; then
#     echo "File already installed"
#     exit 2
# elif [ install_exit_code -eq 0 ]; then
#     echo "Installed successfully"
# # elif [ above_exit_code -ne 0 ]; then
# #     echo "Failed to install"
# #     exit 1
# else
#     echo "Failed to install"
#     exit install_exit_code
# fi
